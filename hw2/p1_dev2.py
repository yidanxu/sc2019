def Scheduler2(L):
    """
    Question 1.1
    Schedule tasks using dependency list provided as input

    Input:
    L: Dependency list for tasks. L contains N sub-lists, and L[i] is a sub-list
    containing integers (the sub-list may also be empty). An integer, j, in this
    sub-list indicates that task j must be completed before task i can be started.

    Output:
    A list of integers corresponding to the schedule of tasks. L[i] indicates
    the day on which task i should be carried out. Days are numbered starting
    from 0.

    Idea:
    We utilise dynamic programming and recursion with hash table to memorise.
    The idea is that we reach the base case and retrieve soluion backwards.
    We iterate through the list of tasks needed to be scheduled, and do the
    operation below if it is not assigned a date.
    The base case is when sub-task list is empty, we update the schedule of the
    task as 0; and if the task has been considered, we continue to the next item
    in the schedule list. Then, for other unconsidered cases, we look at the
    subtasks and make evaluations recursively, until the base case is reached.
    Notably, as the subtask should all been completed before task can be
    implemented, we assign the date for task to be the maximum of its subtask's
    completion date plus one.

    Implementation:
    S: dictionary with key the task index and value the scheduled date
    dSchedule: function that call itself recursively in computing the date for
    unconsidered tasks.
    We then use a loop to check for any task that has not been considered and
    implement dSchedule if not.

    Discussion:
    N the number of tasks, M the number of unique dependencies. P the length of
    dependency list of a task.

    Schedule date if task not considered (the for loop) -----------------
        dSchedule -------------------------------------------------------
            check conditions ---------------------------------------- O(1)
            find maximum of date among subtasks --------------------- O(P)
                apply dSchedule to subtasks ------------------------------

    ----------------------------------------------------------------- O(N*maxP)

    Notably, as we have implemented with a memorise scheme, dSchedule can only
    be applied once to each node. Moreover, if the length of the dependency list
    is small, the operation of finding maximum can be regarded as constant time;
    however, can not be ignored if list is large. Thus, the best case scenario
    of the overall time complexity is O(N), and the in the worst case scenario
    O(N*maxP).
    """
    def dSchedule(task):

        if S[task] != -100: # retun schedule if have computed
            return S[task]
        else:
            if len(L[task]) == 0: # base task
                # update schedule
                S[task] = 0
                return 0
            else:
                S[task] = max([dSchedule(subt) for subt in L[task]]) + 1
                return S[task]

    N = len(L)
    # initialise Schedule
    S = dict([(i, -100) for i in range(N)])
    for key, val in S.items():
        if val == -100:
            dSchedule(key)

    return list(S.values())

def Scheduler3(L):
    """
    Question 1.1
    Schedule tasks using dependency list provided as input

    Input:
    L: Dependency list for tasks. L contains N sub-lists, and L[i] is a sub-list
    containing integers (the sub-list may also be empty). An integer, j, in this
    sub-list indicates that task j must be completed before task i can be started.

    Output:
    S: A list of integers corresponding to the schedule of tasks. L[i] indicates
    the day on which task i should be carried out. Days are numbered starting
    from 0.

    Idea:
    We modify DFS to first sort the list of tasks to be scheduled in a topological
    order, such that the tasks with lower dependency comes before tasks with higher
    dependency in a partial order (e.g. not necessarily the case that all tasks
    with no dependencies are ordered in the beginning). However, we can make sure
    that the subtasks are considered at the time that we have reached task with
    dependencies on those subtasks.

    Implementation:

    Ltopo: sorted list of tasks
    E: set of explored nodes
    S: date assigned to the task

    topo_sort:
    function that takes input task, and make depth first search in its list of
    pre-tasks if the pre-task was not considerd, it is added to the considered
    set E and the function is called again to operate on the dependant tasks of
    the pre-task. The recursion stops until we reach the base case that the
    pre-task list is empty, we then assign the task with empty list to Ltopo.
    With the recursion structure, tasks with higher dependencies are retrived
    after its pre-tasks have been considered.

    The function topo_sort is called for tasks in L if it is not considered.
    After the sorted L is stored in Ltopo, we loop through Ltopo and consider
    tasks from the beginning of the list. Assign task to 0 in S if the pre-task
    list is empty, otherwise find the maximum scheduled date of subtasks plus one.

    Discussion:
    N the number of tasks, M the number of unique dependencies, P the length of
    the sub-task list for each task except for the leaves, for which P is 1.

    intialising S ---------------------------------------------------- O(N)
    topo_sort over list of tasks ------------------------------------- O(N+M)
    task retrieval over Ltopo ---------------------------------------- O(N)
            schedule for task ---------------------------------------- O(P)

    ------------------------------------------------------------------ O(N+M+NmaxP)

    As the list of sub-tasks for each task contains all dependencies, not just
    the sub-tasks with direct dependencies, which in that case is O(N+M) complexity;
    however, there will be repeated values considered as we move up to the higher
    hierachy. Thus the retrieval of scheduled date is larger than O(N+M) and
    smaller than O(N*maxP). The worst case scenario would be O(N+M+NmaxP).
    """
    N = len(L)
    # initialising
    Ltopo = []
    E = set() # explored nodes
    S = [-100] * N

    # find the topology structure of the DAG
    # from leaves to the top, may be with different segements
    def topo_sort(task):
        for subt in L[task]:
            if subt not in E:
                E.add(subt)
                topo_sort(subt)
        Ltopo.append(task)

    for task in range(N):
        if task not in E:
            topo_sort(task)

    # retrieve the structure to assign date to tasks
    while len(Ltopo) > 0:
        task = Ltopo.pop(0)
        if len(L[task]) == 0:
            S[task] = 0
            continue
        S[task] = max([S[subt] for subt in L[task]]) + 1

    return S

def findPath(A,a0,amin,J1,J2):
    """
    Question 1.2 i)
    Search for feasible path for successful propagation of signal from node
    J1 to J2

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element
    tuples (the sub-list my also be empty) of the form (j,Lij). The integer,
    j, indicates that there is a link between nodes i and j and Lij is the
    loss parameter for the link.

    a0: Initial amplitude of signal at node J1

    amin: If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine if the signal can successfully reach node J2
    from node J1

    Output:
    L: A list of integers corresponding to a feasible path from J1 to J2.

    Idea:
    Modify DFS with additional check for signal reached to a node above the
    threshold, i.e. it requires a0*l >= amin; and a stopping creterion that
    breaks the loop when J2 is reached.

    Implementation:
    E: the record of visited nodes
    Lpath: the path between source J1 and every reachable nodes

    DFS: a function that takes the input of a source node and conduct DFS with
    a recursive scheme, while signal aboves threshold and J2 is not reached.
    Index of the node is stored to E when visited. The path is stored and
    updated in Lpath when condition above is met.

    The path from J1 to J2 is returned if J2-th position of Lpath is not empty.

    Discussion: N the number of nodes in the graph, M the number of edges.

    Initialising ---------------------------------------------------- O(N)
    DFS -----------------------------------------------------------------
        for neb, l in A[J1] ----------------------------------------- O(N)
            if neb not in E ----------------------------------------- O(1)
                if a0*l >= amin ------------------------------------- O(1)
                    update E ---------------------------------------- O(1)
                    update path ------------------------------------- O(1)
                    if neb!=J2 -------------------------------------- O(1)
                        DFS ----------------------------------------------
    ----------------------------------------------------------------- O(M+N)
    subset path ----------------------------------------------------- O(1)
    check path exist ------------------------------------------------ O(1)

    ----------------------------------------------------------------- O(M+N)

    Notably, similar as before, the recursive DFS operates in a bottom-up manner,
    in the worst case, we will visit every node and edge in the network, thus
    the overall time complexity is O(M+N), same as the original DFS with recursion.
    """
    size = len(A) # the number of nodes in the network
    E = set()
    Lpath = dict([(i, []) for i in range(size)])

    # initialise the lists
    E.add(J1)
    Lpath[J1] += [J1]

    def DFS(s):
        for neb, l in A[s]:
            if neb not in E: # proceed if neighbour not visited
                if a0*l >= amin: # proceed if signal above threshold and not J2
                    E.add(neb)
                    Lpath[neb] += Lpath[s]
                    Lpath[neb] += [neb]
                    if neb==J2: # break the loop if J2 is reached
                        break
                    else:
                        DFS(neb)
        return Lpath

    # Lpath = DFS(J1)
    L = DFS(J1)[J2]

    if len(L)==0:
        print("No path found for ", J1, "->", J2)
    else:
        return L


def a0min(A,amin,J1,J2):
    """
    Question 1.2 ii)
    Find minimum initial amplitude needed for signal to be able to
    successfully propagate from node J1 to J2 in network (defined by adjacency
    list, A)

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples
    (the sub-list my also be empty) of the form (j,Lij). The integer, j, indicates
    that there is a link between nodes i and j and Lij is the loss parameter for
    the link.

    amin: Threshold for signal boost
    If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine min(a0) needed so the signal can successfully
    reach node J2 from node J1

    Output:
    (a0min,L) a two element tuple containing:
    a0min: minimum initial amplitude needed for signal to successfully reach
    J2 from J1
    L: A list of integers corresponding to a feasible path from J1 to J2 with
    a0=a0min
    If no feasible path exists for any a0, return output as shown below.

    Idea:
    Modified Dijkstra search algorithm with a greedy approach to determine the
    smallest a0 from source node J1 to any other nodes. Notably, in this case,
    as we want to find the minimum a0 such that we can find a path between J1
    and J2, thus a0 >= amin/Lij for all Lij associated with the path. The question
    is then transformed to the following:
    Find a path from source node to each node, where the maximum weight contained
    in the path is the minimum among all possible paths from source to that node.

    The maximum amin/Lij requirement is to gaurantee the path can be traversed
    for the a0min chosen; the 'minimum among all path' requirement gaurantees
    the a0min we find is the smallest, as the original problem required.

    Implementation:
    Edict:
    Dictionary with considered node as key, and a size 2 list containing chosen
    a0min and the corresponding path from source to the node;
    Udict:
    Dictionary with node under consideration as key, and a size 2 list containing
    current smallest maximum a0 and the corresponding path from source to the node;
    nmin: the node in Udict with the minimum a0
    dmin: the corresponding a0

    Append J1 to Udict for consideration.

    The while loop go through Udict to find nmin, node with minimum a0, denoted
    dmin and move to Edict. Stop the search when J2 is chosen and return a0min
    and corresponding path found. Continue if Udict not empty.

    The for loop considers the neighbours of found nmin as above.
    For the neighbours in Udict, update the smallest maximum a0:
    Compare the a0 for each neighbour with the largest value between amin/Lij
    and dmin (this gaurantees we compare the maximum a0 on the path to the a0
    of neighbour); update if the latter is smaller (this gaurantees that we
    always store the smallest maximum a0).
    For the neighbours have been considered (in Edict), pass.
    For the neighbours in neither of the dictionaries, add to Udict.

    Finally, if we have not found a a0min with corresponding path, return -1
    and an empty list.

    Discussion:
    N the number of nodes in the graph, M the number of edges.

    while Udict is not empty --------------------------------------- O(N)
        extract nmin ----------------------------------------------- O(N)
        move nmin from Udict to Edict ------------------------------ O(1)
        check if J2 is chosen -------------------------------------- O(1)
        update smallest maximum a0 --------------------------------- O(M)

    ---------------------------------------------------------------- O(N*(N+M))

    For our simplist implementation, we naively choose the closest unexplored node,
    in the worst case scenario, where J2 is moved to Edict after all nodes have
    been considered, we have O(N**2+MN) overall time complexity.
    This can be improved by arranging nodes in Udict in a binary heap scheme,
    where the nodes are arranged acoording to the ascending order of smallest
    maximum a0 for each node. This allows efficient extraction for nmin and update
    smallest maximum a0.
    When nmin is popped from the heap, the list is re-ordered in O(log2N) time,
    similarly for the reordering after the smallest maximum a0 has been updated,
    the list is reordered again in O(log2N) time. Thus the overall time complexity
    becomes O(log2N*(N+M)), which is more efficient comparing to our naive way of
    storing Udict.
    """

    dinit = 10**6
    Edict = {}
    Udict = {}

    Udict[J1] = [0, [J1]] # start from J1, weight indicates smallest maximum

    while len(Udict)>0:
        # Find node with min a0min in Udict and move to Edict
        dmin = dinit
        for n, w in Udict.items():
            if w[0] < dmin:
                dmin = w[0]
                nmin = n
        Edict[nmin] = Udict.pop(nmin)
        if nmin == J2: # stop the search when reached J2 and return path from J1-J2
            return dmin, Edict.get(nmin)[1]

        # Update smallest maximum a0 with weight amin/Lij
        for n, l in A[nmin]:
            # propose a new connection to the neighbours
            dcomp = max(amin/l, dmin)
            if n in Udict:
                if dcomp < Udict[n][0]:
                    Udict[n] = [dcomp, Edict[nmin][1] + [n]]
            elif n in Edict:
                pass
            else: # initialise unseen and unvisited neighbours
                Udict[n] = [dcomp, Edict[nmin][1] + [n]]

    if J2 not in Edict.keys():
        return -1, []
