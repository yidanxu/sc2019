A = [[(1, 0.7)], [(0, 0.7), (2, 0.6)], [(1, 0.6), (3, 0.3)], [(2, 0.6), (4, 0.5)], [(3, 0.3)]]
amin = 0.5
J1 = 0
J2 = 4
a0 = 1

size = len(A)
E = set()
Lpath = dict([(i, []) for i in range(size)])

# initialise the lists
E.add(J1)
Lpath[J1] += [J1]

def DFS(s):
    flag = False
    for neb, l in A[s]:
        if neb not in E: # proceed if neighbour not visited
            E.add(neb)
            Lpath[neb] += Lpath[s]
            if neb==J2: # break the loop if J2 is reached
                Lpath[neb] += [neb]
                break
            elif a0*l >= amin: # proceed if signal above threshold and not J2
                Lpath[neb] += [neb]
                DFS(neb)
            else: # neighbor not reachable, back to s
                Lpath[neb] += ["died"]
    return Lpath

L = DFS(J1)[J2]

if J2 not in E:
    print("No path found")
else:
    print(L)

A = [[(1, 0.4), (3, 0.7)], [(0, 0.4), (2, 0.5)],
    [(1, 0.5), (4, 0.3)], [(0, 0.7), (4, 0.5)],
    [(3, 0.5), (2, 0.3)]]
