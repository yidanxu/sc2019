"""M345SC Homework 2, part 2, Question 3
Your name and CID here
"""
import numpy as np
import networkx as nx
from scipy.integrate import odeint
import scipy.sparse as sp
import matplotlib.pyplot as plt

def modelN2(G,eval,x=0,params=(0,80,0,0,0,0.01),tf=6,Nt=400):
    """
    Question 3

    Input:
    G: Networkx graph
    eval: the indicator of subplot position in a row
    params: contains model parameters defined as in Question 2, with only theta0
    and tau non-zero values, which are chosen as desired.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    x: node which is initially infected (applied to fig3 and fig4)

    output:
    The value of population proportion of each state S, I, V at each time step
    for nodes in network G. Depending on the subplot position in a row, out put
    either the linear diffusion model or simplified infection model.
    """
    # initialising
    a, theta0, theta1, g, k, tau = params
    tarray = np.linspace(0,tf,Nt+1)

    # obtain from network G the initial status and node degree
    degree = np.array(list(G.nodes.data('degree'))).T[1, :]
    status = np.array(list(dict(G.nodes.data('status')).values())).T
    y0 = status.flatten()
    N = int(len(y0)/3)

    # notably, A and D are in csr sparse matrix format
    A = nx.adjacency_matrix(G)
    D = sp.diags(degree, 0).tocsr()
    # laplacian for linear diffusion
    Lal = tau*(A - D)

    # compute Flux matrix also stored as csr sparse matrix
    num = D.dot(A)
    denom = tau/(A.dot(degree))
    # flux matrix
    F = num.multiply(denom)
    # laplacian for flux incorporating node degree
    Law = F - sp.diags(np.ones(N)*tau, 0).tocsr()

    def RHSl(y,t):
        """ Compute RHS of linear diffusion model at time t
        input: y should be a 3N 1-d array containing with
        y[:N],y[N:2*N],y[2*N:3*N] corresponding to
        S on nodes 0 to N-1, I on nodes 0 to N-1, and
        V on nodes 0 to N-1, respectively.
        output: dy: also a 3N 1-d array corresponding to dy/dt
        """
        # initialising
        St, It, Vt = y[:N], y[N:2*N], y[2*N:]

        # no interaction between states for linear diffusion
        dS = Lal.dot(St)
        dI = Lal.dot(It)
        dV = Lal.dot(Vt)

        dy = np.array([dS, dI, dV]).flatten()

        return dy

    def RHSw(y,t):
        """ Compute RHS of simplfied infection model at time t
        input: y should be a 3N 1-d array containing with
        y[:N],y[N:2*N],y[2*N:3*N] corresponding to
        S on nodes 0 to N-1, I on nodes 0 to N-1, and
        V on nodes 0 to N-1, respectively.
        output: dy: also a 3N 1-d array corresponding to dy/dt
        """
        # initialising
        St, It, Vt = y[:N], y[N:2*N], y[2*N:]

        # simplfied infection model as in Question 2
        theta = theta0 + theta1*(1 - np.sin(2*np.pi*t))
        # use dot product from csr sparse matrix
        dS = a*It - (g + k)*St + Law.dot(St)
        dI = theta*St*Vt - (k + a)*It + Law.dot(It)
        dV = k*(1 - Vt) - theta*St*Vt + Law.dot(Vt)

        dy = np.array([dS, dI, dV]).flatten()

        return dy

    # output depending on index in a given row
    if eval%2==1:
        return odeint(RHSl, y0, tarray)
    else:
        return odeint(RHSw, y0, tarray)


def plot_theta(G,x,nvar,var,n,tarray,pos,theta_lis=np.linspace(0,99,10)):
    """ Plot fig3
    input:
    G: Networkx graph
    x: node which is initially infected
    nvar: the index of the state (S, I, V)
    var: state
    n: the size of network G
    tarray: time step
    pos: the index of subgraph position (1-12)
    theta_lis: the 10 theta0 considered
    """
    plt.subplot(3,4,pos)
    eval = pos%4
    # in the first and second column (linear diffusion)
    if eval==1 or eval==2:
        # plot mean of a given state values over time for varying theta0
        for theta in theta_lis:
            params=(0, theta, 0, 0, 0, 0.01)
            Diff = modelN2(G,eval,x,params)
            plt.plot(tarray, np.mean(Diff[:, nvar*n:(nvar+1)*n], axis=1),
                        label=r"$\theta_0=${}".format(theta))
        plt.ylabel('{} mean'.format(var))
        plt.legend(loc=0, ncol=3)
    # interactive dynamics of the simplfied infection model
    else:
        # plot variance of a given state values over time for varying theta0
        for theta in theta_lis:
            params=(0, theta, 0, 0, 0, 0.01)
            Diff = modelN2(G,eval,x,params)
            plt.plot(tarray, np.var(Diff[:, nvar*n:(nvar+1)*n], axis=1),
                        label=r"$\theta_0=${}".format(theta))
        plt.ylabel('{} variance'.format(var))
        # plt.legend(loc=0, ncol=4, fontsize='xx-small')

def plot_tau(G,x,nvar,var,n,tarray,pos,tau_lis=np.linspace(0,1,10)):
    """ Plot fig4
    input:
    G: Networkx graph
    x: node which is initially infected
    nvar: the index of the state (S, I, V)
    var: state
    n: the size of network G
    tarray: time step
    pos: the index of subgraph position (1-12)
    tau_lis: the 10 diffusivity parameter tau considered
    """
    plt.subplot(3,4,pos)
    eval = pos%4
    # in the first and second column (linear diffusion)
    if eval==1 or eval==2:
        # plot mean of a given state values over time for varying tau
        for tau in tau_lis:
            params = (0, 80, 0, 0, 0, tau)
            Diff = modelN2(G,eval,x,params)
            plt.plot(tarray, np.mean(Diff[:, nvar*n:(nvar+1)*n], axis=1),
                        label=r"$\tau=${}".format(round(tau,2)))
        plt.ylabel('{} mean'.format(var))
        plt.legend(loc=0, ncol=3)
    # interactive dynamics of the simplfied infection model
    else:
        # plot variance of a given state values over time for varying tau
        for tau in tau_lis:
            params = (0, 80, 0, 0, 0, tau)
            Diff = modelN2(G,eval,x,params)
            plt.plot(tarray, np.var(Diff[:, nvar*n:(nvar+1)*n], axis=1),
                        label=r"$\tau=${}".format(round(tau,2)))
        plt.ylabel('{} variance'.format(var))
        # plt.legend(loc=0, ncol=4, fontsize='xx-small')

def plot_x0(G,x,nvar,var,n,tarray,pos,node_lis,params):
    """ Plot fig6
    input:
    G: Networkx graph
    x: node which is initially infected
    nvar: the index of the state (S, I, V)
    var: state
    n: the size of network G
    tarray: time step
    pos: the index of subgraph position
    node_lis: the 10 initially-infected nodes with varying degree considered
    """
    plt.subplot(3,4,pos)
    eval = pos%4
    # in the first and second column (linear diffusion)
    if eval==1 or eval==2:
        # plot mean of a given state values over time for varying x0
        for x in node_lis:
            G.nodes[x]['status'] = [0.05, 0.05, 0.1]
            deg = G.nodes[x]['degree']
            Diff = modelN2(G,eval,x,params)
            # reset G
            G.nodes[x]['status'] = [0, 0, 1]
            plt.plot(tarray, np.mean(Diff[:, nvar*n:(nvar+1)*n], axis=1),
                        label="x={0},deg={1}".format(x,deg))
        plt.ylabel('{} mean'.format(var))
        plt.legend(loc=0, ncol=3)
    # interactive dynamics of the simplfied infection model
    else:
        # plot variance of a given state values over time for varying x0
        for x in node_lis:
            G.nodes[x]['status'] = [0.05, 0.05, 0.1]
            deg = G.nodes[x]['degree']
            Diff = modelN2(G,eval,x,params)
            G.nodes[x]['status'] = [0, 0, 1]
            plt.plot(tarray, np.var(Diff[:, nvar*n:(nvar+1)*n], axis=1),
                        label="x={0},deg={1}".format(x,deg))
        plt.ylabel('{} variance'.format(var))
        # plt.legend(loc=0, ncol=4, fontsize='xx-small')

def diffusion(n=100,m=5,seed=111,params=(0, 80, 0, 0, 0, 0.01),tf=6,Nt=400):
    """ Analyze similarities and differences
    between simplified infection model and linear diffusion on Barabasi-Albert networks.

    Input:
    n: the number of nodes of a generated BA graph
    m: the number of edges attached when introducing a new node into the network
    seed: fix the generated network
    params: contains parameters for infection model defined as in Question 2,
    with only theta0 and tau non-zero values, which are chosen as desired.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)

    Display:
    For each generated plot, the first two columns shows the mean of S,I,V for linear
    diffusion model and simplified infection model respectively.

    - fig3: the plot of mean and variance of S, I and V against time for both
    linear diffusion and simplfied infection model discussed as in Question 2, while varying
    theta0 (the rate of converging from vulnerable cells to infected cells) from 0
    to 100 by stepsize 10.

    - fig4: the plot of mean and variance of S, I and V against time
    linear diffusion and simplfied infection model discussed as in Question 2, while varying
    tau (the diffusivity parameter) from 0 to 1 by step size 0.1

    - fig5: the plot of mean and variance of S, I and V against time
    linear diffusion and simplfied infection model discussed as in Question 2, while varying
    the initially-infected nodes with varying degrees.

    Discussion:
    We compare the similarities and differences between linear diffusion and simplified
    infection model by varying theta0 (conversion rate from vulnerable cell to infected
    cells), tau (the diffusivity parameter), and x0 (the initially-infected cell). We
    investigate the mean and variance at each time step for each states over the entire
    network G. By observing the trends in fig3, fig4, fig5, we identify the following
    three key points respectively to each plot.

    1. Linear diffusion does not involve interactions between states, with no conversion
    from one state to another, and just the flow of cells at each state between nodes,
    i.e. it is not controlled by theta0. This gives constant mean and decaying variances
    of S,I,V for the linear diffusion model.

    On the other hand, the simplified infection modle has only S diffusing linearly when
    theta0 not 0, i.e. there only exists conversions between vulnerable cells and infected cells.
    However, we should note that, with recovery rate g set to be zero, the total number
    of cells at states I and V are preserved over the network G; moreover, with theta0>0,
    V monotonically increases and I monotonically decreases when tau>0.
    Moreover, unlike linear diffusion, the variance of change of proportion of cells at
    state I and V increases, due to the positive theta
    (can also talk about the change of mean and variance of I and V for increasing theta0)

    3.
    - For linear diffusion:
    The higher the diffusivity parameter, the faster the variance decays for S,I,V.
    - For simplified infection model:
    The higher the diffusivity parameter, the faster the variance decays for S, which
    is not as fast as the linear diffusion model; similarly, the faster the variance
    increases for I. Interestingly, the variance of V changes faster for higher tau,
    the trend is first increase then decrease.
    Again, we see that the main similarity between linear diffusion and the simplified
    infection model lies in the change of mean of S, which is always zero, as it is
    preserved in both cases.

    2.
    For simplified infection model, there is no monotonic relation between the increase
    of variance and the increase of degree. By the construction of our diffusion matrix,
    the flux not only depends on the degree of the out-flow node itself, but also the
    total degree of the in-flow cell's neighbours, which possibly includes the out-flow
    cell and others. This is the possible attributor to the similar change of variance
    for varying intially-infected cells (with deifferent degrees); by construst, the
    variance of state S of linear diffusion model does depend on the degree of x0, and
    it decreases almost linearly while time progresses, which is similar in the case of
    varying theta0.
    """
    tarray = np.linspace(0,tf,Nt+1)
    # BA network with n nodes and m edges added when ever a new node is introduced
    G = nx.barabasi_albert_graph(n, m, seed)

    # the dictionary of node attributes, a list containing the S, I, V
    stat_dict = {k:[0, 0, 1] for k in list(G.nodes())}
    nx.set_node_attributes(G, stat_dict, 'status')
    # add degree to the network as well
    nx.set_node_attributes(G, dict(G.degree()), 'degree')

    # randomly choosing initially-infected node for fig3 and fig4
    x = np.random.choice(100, 1)[0]
    G.nodes[x]['status'] = [0.05, 0.05, 0.1]

    # configurarte plot parameters
    pltparams = {'legend.fontsize': 'xx-small',
         'axes.labelsize': 'x-small',
         'axes.titlesize':'x-small',
         'xtick.labelsize':'xx-small',
         'ytick.labelsize':'xx-small'}
    plt.rcParams.update(pltparams)

    """ We first look at the difference of mean and variance between linear diffusion
    and simplified infection model for varying conversion rate between infected cells
    and vulnerable cells."""
    plt.rcParams["figure.figsize"] = [20,10]
    plt.figure()
    for eval in range(4):
        for nvar in range(3):
            pos = 4*nvar + eval + 1
            var = ["S", "I", "V"][nvar]
            plot_theta(G,x,nvar,var,n,tarray,pos)
        if eval%2==0:
            plt.xlabel('time step/ Linear diffusion with x0={0} with degree {1}'.format(x, G.nodes[x]['degree']))
        else:
            plt.xlabel('time step/ simplfied infection model with x0={0} with degree {1}'.format(x, G.nodes[x]['degree']))
    plt.tight_layout()
    plt.subplots_adjust(top=0.92)
    plt.suptitle(r"Change of $\theta_0$ over BA Network ({0},{1}); Yidan Xu; modelN2, plot_theta".format(n,m))
    plt.savefig("fig3.png")
    plt.show()
    plt.close()


    """ Then, we investigate for the change of tau and a fixed theta0=80 as the
    rate of change from vulnerable to infected cells, comparing linear diffusion model
    and simplified infection model."""
    plt.rcParams["figure.figsize"] = [20,10]
    plt.figure()
    for eval in range(4):
        for nvar in range(3):
            pos = 4*nvar + eval + 1
            var = ["S", "I", "V"][nvar]
            plot_tau(G,x,nvar,var,n,tarray,pos)
        if eval%2==0:
            plt.xlabel('time step/ Linear diffusion with x0={0} with degree {1}'.format(x, G.nodes[x]['degree']))
        else:
            plt.xlabel('time step/ simplfied infection model with x0={0} with degree {1}'.format(x, G.nodes[x]['degree']))
    plt.tight_layout()
    plt.subplots_adjust(top=0.92)
    plt.suptitle(r"Change of Diffusivity $\tau$ over BA Network ({0},{1}); Yidan Xu; modelN, plot_tau".format(n,m))
    plt.savefig("fig4.png")
    plt.show()
    plt.close()

    """ We now try to simulate with different initially-infected nodes, and compare
    the mean and variances between linear diffusion model and simplified infection
    model."""
    node_lis = np.array(G.nodes())[:10]
    # reset G
    G.nodes[x]['status'] = [0, 0, 1]

    plt.rcParams["figure.figsize"] = [20,10]
    plt.figure()
    for eval in range(4):
        for nvar in range(3):
            pos = 4*nvar + eval + 1
            var = ["S", "I", "V"][nvar]
            plot_x0(G,x,nvar,var,n,tarray,pos,node_lis,params)
        if eval%2==0:
            plt.xlabel('time step/ Linear diffusion')
        else:
            plt.xlabel('time step/ simplfied infection model')
    plt.tight_layout()
    plt.subplots_adjust(top=0.92)
    plt.suptitle(r"Change of initially-infected node over BA Network ({0},{1}); Yidan Xu; modelN2, plot_x0".format(n,m))
    plt.savefig("fig5.png")
    plt.show()
    plt.close()


if __name__=='__main__':
    n=100
    m=5
    seed=111
    params=(0, 80, 0, 0, 0, 0.01)
    tf=6
    Nt=400

    diffusion(n,m,seed,params,tf,Nt)
