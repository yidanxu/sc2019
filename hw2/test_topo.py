def topo_sort(task):
    for subt in L[task]:
        if subt not in E:
            E.add(subt)
            topo_sort(subt)
    Ltopo.append(task)
    print(Ltopo)
    print(E)


L = [[1, 2, 3, 4, 5, 6], [], [1], [], [3, 6], [], [], [8, 9], [], [8], [7, 8, 9]]
N = len(L)
Ltopo = []
# explored nodes
E = set()
S = [-100] * N

for task in range(N):
    if task not in E:
        print("task", task, "not in ")
        topo_sort(task)

while len(Ltopo) > 0:
    print("Ltopo is", Ltopo)
    task = Ltopo.pop(0)
    print("process task", task)
    if len(L[task]) == 0:
        # initialise the list whenever we reach a base case
        e = [0]
        print("find basecase", task)
        S[task] = 0
        continue

    # update the topological ordering
    e.append((max(e) + 1))
    print(e)
    S[task] = e[-1]

""" Alternative retrieval methods
Notice that we may lose the ordering when
the last task is connected to a base node;
We instead consider updating the day for completion by taking the max of subtasks + 1
"""
while len(Ltopo) > 0:
    task = Ltopo.pop(0)
    if len(L[task]) == 0:
        S[task] = 0
        continue

    S[task] = max([S[subt] for subt in L[task]]) + 1
