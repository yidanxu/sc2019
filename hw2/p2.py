"""M345SC Homework 2, part 2
Name: Yidan Xu
CID: 01205667
"""
import numpy as np
import networkx as nx
from scipy.integrate import odeint
import scipy.sparse as sp
import matplotlib.pyplot as plt


def model1(G,x=0,params=(50,80,105,71,1,0),tf=6,Nt=400,display=False):
    """
    Question 2.1
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    S: Array containing S(t) for infected node

    Display:
    If `display=True`, generate a plot of the proportion of Spreaders at node
    x over the time range from 0 to tf. (i.e. plot S against tarray.)
    """
    a, theta0, theta1, g, k, tau = params
    tarray = np.linspace(0,tf,Nt+1)

    # systems of ODEs to be solved
    def RHS(z, t):
        S, I, V = z

        theta = theta0 + theta1*(1 - np.sin(2*np.pi*t))
        dS = a*I - (g + k)*S
        dI = theta*S*V - (k + a)*I
        dV = k*(1 - V) - theta*S*V

        dz = [dS, dI, dV]
        return dz

    # initial condition at node x
    z0 = G.node[x]['status']

    # solve ODEs
    S = odeint(RHS, z0, tarray)[:, 0]

    if display:
        # set figure size
        plt.rcParams["figure.figsize"] = [8,5]
        # plot the result of S at each time step
        plt.plot(tarray, S,
        label=r'$\frac{{dS_{0}}}{{dt}} = \alpha I_{0} \; - \; (\gamma+\kappa)S_{0}$'
                         .format(x))

        plt.ylabel(r'$S_{{{0}}}$'.format(x))
        plt.xlabel('time step')
        plt.legend(loc='best')
        plt.title(
                "S at each time step of infected node x={}; Yidan Xu; model1".format(x))
        plt.savefig("fig3.png")
        plt.show()
        plt.close()

    return S


def modelN(G,x=0,params=(50,80,105,71,1,0.01),tf=6,Nt=400,display=False):
    """
    Question 2.1
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    Smean,Svar: Array containing mean and variance of S across network nodes at
    each time step.
    """
    # initialising
    a, theta0, theta1, g, k, tau = params
    tarray = np.linspace(0,tf,Nt+1)

    # obtain from network G the initial status and node degree
    degree = np.array(list(G.nodes.data('degree'))).T[1, :]
    status = np.array(list(dict(G.nodes.data('status')).values())).T
    y0 = status.flatten()
    N = int(len(y0)/3)

    # notably, A and D are in csr sparse matrix format
    A = nx.adjacency_matrix(G)
    D = sp.diags(degree, 0).tocsr()

    # compute Flux matrix also stored as csr sparse matrix
    num = D.dot(A)
    denom = tau/(A.dot(degree))

    F = num.multiply(denom)
    La = F - sp.diags(np.ones(N)*tau, 0).tocsr()

    def RHS(y,t):
        """Compute RHS of model at time t
        input: y should be a 3N x 1 array containing with y[:N],y[N:2*N],
        y[2*N:3*N] corresponding to
        S on nodes 0 to N-1, I on nodes 0 to N-1, and
        V on nodes 0 to N-1, respectively.
        output: dy: also a 3N x 1 array corresponding to dy/dt

        Implementation:
        The RHS of the equation is vectorised and implemented with sparse
        matrix in scipy library. The running time is significantly improved
        for large networks.

        The RHS function compute the update of St,It,Vt for each time step,
        with the system of ODE specified as below expression of dS, dI and
        dV. Further more, the summation is rewritten in the form of matrix
        multiplication of (F-T) and size N 1-d array S, I, V at a given time
        step t respectively, where T is the diagonal matrix with constant
        tau on the diagonal.
        Take the operation over Spreaders S as an example:
        This can be specified by moving Si outside of the second summation
        over j, and the sum of Fji over j is simply the diffusivity parameter tau
        for all i; then dS is exactly TS. We can rewrite the first summation
        as matrix multiplication of F and S, i.e. FS. Thus the RHS summation term
        for S is simply (F-T)S. Similarly, for other status I and V.

        Discussion:
        The estimate of the number of operations required to compute dSi/dt
        for i from 0 to N-1 in one call of RHS:

        slicing and asigning the input S,I at time step t-1 in y  2N+2
        a*It ----------------------------------------------------- N
        (g + k)*St ----------------------------------------------- N+1
        La.dot(St) ----------------------------------------------- 3N+4M+c
        array summation ------------------------------------------ 2N

        ---------------------------------------------------------- 9N+4M+c

        Firstly, we note that operations on arraies are element-wise thus
        time complexity is O(N) for multiplications and summations.
        Notably, in a csr (Compressed Sparse Row) sparse matrix, non-zero
        elements are stored as three 1-d arraies (A, AI, AJ), representing
        the value, accumulative sum of non-zero entries in each row (read
        before column) and column index respectively.
        In our case, the non-zero elements represents the edges in the
        graph, and notably, both A and AJ has length M and AI has length N+1
        (the first entry is always zero, and the last entry is always M).
        When doing matrix-vector multiplication, the values of the ith row
        of original matrix is read from A[AI[i]:AI[i+1]], which requires
        N+M+2N operations. Similarly, the column index of the value is
        subsetted along with the corresponding entries in the vector St,
        which requires 2M operations. Then element-wise multiplication and
        summation are performed, which require additional M+c operations
        to give the final result. Thus 9N+4M+c operations in total.
        """
        # initialising
        St, It, Vt = y[:N], y[N:2*N], y[2*N:]

        theta = theta0 + theta1*(1 - np.sin(2*np.pi*t))
        # use dot product from csr sparse matrix
        dS = a*It - (g + k)*St + La.dot(St)
        dI = theta*St*Vt - (k + a)*It + La.dot(It)
        dV = k*(1 - Vt) - theta*St*Vt + La.dot(Vt)

        dy = np.array([dS, dI, dV]).flatten()

        return dy

    # now we need to solve the ODE for 3N variables
    # each for Nt number of times using odeint
    S = odeint(RHS, y0, tarray)[:, :N]
    Smean = np.mean(S, axis=1)
    Svar = np.var(S, axis=1)

    if display:
        # set figure size
        plt.rcParams["figure.figsize"] = [8,5]
        # plot the mean of S at each time step over all nodes
        plt.plot(tarray, Smean)
        plt.ylabel('Mean of S')
        plt.xlabel('time step/ node x={0} with degree {1}'.format(x, G.nodes[x]['degree']))
        plt.title(
                "Mean of S over all nodes of G at each time step; Yidan Xu; modelN")
        plt.savefig("fig4.png")
        plt.show()
        plt.close()

        # plot the variance of S at each time step over all nodes
        plt.rcParams["figure.figsize"] = [8,5]
        plt.plot(tarray, Svar)
        # plt.rcParams["figure.figsize"] = [9,6]
        plt.ylabel('Variance of S')
        plt.xlabel('time step/ node x={0} with degree {1}'.format(x, G.nodes[x]['degree']))
        plt.title(
                "Variance of S over all nodes of G at each time step; Yidan Xu; modelN")
        plt.savefig("fig5.png")
        plt.show()
        plt.close()

    return Smean, Svar


""" Question 3
"""

def modelN2(G,eval,x=0,params=(0,80,0,0,0,0.01),tf=100,Nt=1000):
    """
    Question 3

    Input:
    G: Networkx graph
    eval: the indicator of subplot position in a row
    params: contains model parameters defined as in Question 2, with only theta0
    and tau non-zero values, which are chosen as desired.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    x: node which is initially infected (applied to fig3 and fig4)

    output:
    The value of population proportion of each state S, I, V at each time step
    for nodes in network G. Depending on the subplot position in a row, out put
    either the linear diffusion model or simplified infection model.
    """
    # initialising
    a, theta0, theta1, g, k, tau = params
    tarray = np.linspace(0,tf,Nt+1)

    # obtain from network G the initial status and node degree
    degree = np.array(list(G.nodes.data('degree'))).T[1, :]
    status = np.array(list(dict(G.nodes.data('status')).values())).T
    y0 = status.flatten()
    N = int(len(y0)/3)

    # notably, A and D are in csr sparse matrix format
    A = nx.adjacency_matrix(G)
    D = sp.diags(degree, 0).tocsr()
    # laplacian for linear diffusion
    Lal = tau*(A - D)

    # compute Flux matrix also stored as csr sparse matrix
    num = D.dot(A)
    denom = tau/(A.dot(degree))
    # flux matrix
    F = num.multiply(denom)
    # laplacian for flux incorporating node degree
    Law = F - sp.diags(np.ones(N)*tau, 0).tocsr()

    def RHSl(y,t):
        """ Compute RHS of linear diffusion model at time t
        input: y should be a 3N 1-d array containing with
        y[:N],y[N:2*N],y[2*N:3*N] corresponding to
        S on nodes 0 to N-1, I on nodes 0 to N-1, and
        V on nodes 0 to N-1, respectively.
        output: dy: also a 3N 1-d array corresponding to dy/dt
        """
        # initialising
        St, It, Vt = y[:N], y[N:2*N], y[2*N:]

        # no interaction between states for linear diffusion
        dS = Lal.dot(St)
        dI = Lal.dot(It)
        dV = Lal.dot(Vt)

        dy = np.array([dS, dI, dV]).flatten()

        return dy

    def RHSw(y,t):
        """ Compute RHS of simplfied infection model at time t
        input: y should be a 3N 1-d array containing with
        y[:N],y[N:2*N],y[2*N:3*N] corresponding to
        S on nodes 0 to N-1, I on nodes 0 to N-1, and
        V on nodes 0 to N-1, respectively.
        output: dy: also a 3N 1-d array corresponding to dy/dt
        """
        # initialising
        St, It, Vt = y[:N], y[N:2*N], y[2*N:]

        # simplfied infection model as in Question 2
        theta = theta0 + theta1*(1 - np.sin(2*np.pi*t))
        # use dot product from csr sparse matrix
        dS = a*It - (g + k)*St + Law.dot(St)
        dI = theta*St*Vt - (k + a)*It + Law.dot(It)
        dV = k*(1 - Vt) - theta*St*Vt + Law.dot(Vt)

        dy = np.array([dS, dI, dV]).flatten()

        return dy

    # output depending on index in a given row
    if eval%2==1:
        return odeint(RHSl, y0, tarray) # linear diffusion
    else:
        return odeint(RHSw, y0, tarray) # simplified infection


def plot_theta(G,x,nvar,var,n,tarray,pos,theta_lis=np.linspace(0,99,10)):
    """ Plot fig0
    input:
    G: Networkx graph
    x: node which is initially infected
    nvar: the index of the state (S, I, V)
    var: state
    n: the size of network G
    tarray: time step
    pos: the index of subgraph position (1-12)
    theta_lis: the 10 theta0 considered
    """
    plt.subplot(3,4,pos)
    eval = pos%4
    # in the first and second column (linear diffusion)
    if eval==1 or eval==2:
        # plot mean of a given state values over time for varying theta0
        for theta in theta_lis:
            params=(0, theta, 0, 0, 0, 0.01)
            Diff = modelN2(G,eval,x,params)
            plt.plot(tarray, np.mean(Diff[:, nvar*n:(nvar+1)*n], axis=1),
                        label=r"$\theta_0=${}".format(theta))
        plt.ylabel('{} mean'.format(var))
        plt.legend(loc=0, ncol=3)
    # interactive dynamics of the simplfied infection model
    else:
        # plot variance of a given state values over time for varying theta0
        for theta in theta_lis:
            params=(0, theta, 0, 0, 0, 0.01)
            Diff = modelN2(G,eval,x,params)
            plt.plot(tarray, np.var(Diff[:, nvar*n:(nvar+1)*n], axis=1),
                        label=r"$\theta_0=${}".format(theta))
        plt.ylabel('{} variance'.format(var))
        # plt.legend(loc=0, ncol=4, fontsize='xx-small')

def plot_tau(G,x,nvar,var,n,tarray,pos,tau_lis=np.linspace(0,1,10)):
    """ Plot fig1
    input:
    G: Networkx graph
    x: node which is initially infected
    nvar: the index of the state (S, I, V)
    var: state
    n: the size of network G
    tarray: time step
    pos: the index of subgraph position (1-12)
    tau_lis: the 10 diffusivity parameter tau considered
    """
    plt.subplot(3,4,pos)
    eval = pos%4
    # in the first and second column (linear diffusion)
    if eval==1 or eval==2:
        # plot mean of a given state values over time for varying tau
        for tau in tau_lis:
            params = (0, 80, 0, 0, 0, tau)
            Diff = modelN2(G,eval,x,params)
            plt.plot(tarray, np.mean(Diff[:, nvar*n:(nvar+1)*n], axis=1),
                        label=r"$\tau=${}".format(round(tau,2)))
        plt.ylabel('{} mean'.format(var))
        plt.legend(loc=0, ncol=3)
    # interactive dynamics of the simplfied infection model
    else:
        # plot variance of a given state values over time for varying tau
        for tau in tau_lis:
            params = (0, 80, 0, 0, 0, tau)
            Diff = modelN2(G,eval,x,params)
            plt.plot(tarray, np.var(Diff[:, nvar*n:(nvar+1)*n], axis=1),
                        label=r"$\tau=${}".format(round(tau,2)))
        plt.ylabel('{} variance'.format(var))
        # plt.legend(loc=0, ncol=4, fontsize='xx-small')

def plot_t(S,n,t,eval):
    """ Plot fig3
    S: time step by n array Si value for i=1,...,n
    n: the size of the network G
    t: index of the time step
    eval: indicates the plotting of linear diffusion model or simplified infection
    model """

    # eval is 1 for linear diffusion; 2 for simplified model
    plt.subplot(4,3,t+eval*6-5)
    plt.plot(np.arange(n), S[t*80, :n])
    plt.ylim((-0.001,0.05))
    plt.xlabel("node_index/t_index={}".format(t*80))


def diffusion(n=100,m=5,seed=111,params=(0, 80, 0, 0, 0, 0.01),tf=100,Nt=1000):
    """ Analyze similarities and differences
    between simplified infection model and linear diffusion on Barabasi-Albert networks.

    Input:
    n: the number of nodes of a generated BA graph
    m: the number of edges attached when introducing a new node into the network
    seed: fix the generated network
    params: contains parameters for infection model defined as in Question 2,
    with only theta0 and tau non-zero values, which are chosen as desired.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)

    Display:
    For each generated plot, the first two columns shows the mean of S,I,V for linear
    diffusion model and simplified infection model respectively.

    - fig0: the plot of mean and variance of S, I and V against time for both
    linear diffusion and simplfied infection model discussed as in Question 2, while varying
    theta0 (the rate of converging from vulnerable cells to infected cells) from 0
    to 100 by stepsize 10.

    - fig1: the plot of mean and variance of S, I and V against time
    linear diffusion and simplfied infection model discussed as in Question 2, while varying
    tau (the diffusivity parameter) from 0 to 1 by step size 0.1

    - fig2: the plot of S across the network at specific 6 time points for linear diffusion
    and simplified infection model.

    Discussion:
    We compare the similarities and differences between linear diffusion and simplified
    infection model by varying theta0 (conversion rate from vulnerable cell to infected
    cells), tau (the diffusivity parameter), and x0 (the initially-infected cell). We
    investigate the mean and variance at each time step for each states over the entire
    network G. By observing the trends in fig0, fig1, fig2, we identify the following
    three key points respectively to each plot.

    1. Linear diffusion does not involve interactions between states, with no conversion
    from one state to another, and it just involves the flow of cells at each state
    between nodes, i.e. it is not controlled by theta0.
    This gives constant mean and decaying variances of S,I,V for the linear diffusion
    model, which can be seen from fig0.
    The simplified infection modle also has S diffusing linearly when theta0 not 0
    (i.e. when there does not only exist conversions from vulnerable cells to infected
    cells). And comparing to the linear diffusion model, we see a slower decay of
    variance of S over time.
    This also can be seen from fig2, where the peak decays faster for linear
    diffusion model.

    2. The dynamics of I and V for linear diffusion and simplified infection model
    presents great differences. (We can )
    For the linear diffusion model:
    It is not affected by the interactions between I and V, and thus, the dynamics of
    the three states behave similarly in terms of constant mean and decaying variances.
    For the infection model:
    We should also note that, with recovery rate g set to be zero, the total number
    of cells at states I and V are preserved over the network G.
    In addition, we observe more dynamics when theta is non-zero, the vulnerable cells
    are converted to infected cells (in a single direction), which attributes to the
    growing mean of I and decaying mean of V. Moreover, as there are less vulnerable
    cells as time progresses across the network, the variance of V reaches a maximum
    then decreases. (In both fig0 and fig1).

    3. In fig2, we can observe that initially both models have a peak at a node of the
    index x. As time progresses, some bumps start to appear in addition to the peak
    in the case of linear diffusion model; whereas, in the case of simplified infection
    model, it appears that the bumps are larger and less spread out apart from the peak.
    This may be attributed to the consideration of node degree when deriving flux matrix
    for the infection model, while the simple linear diffusion only accounts for the
    adjacency matrix (i.e. if there is a path between node i and j). However, we do note
    that this effect would be subject to the randomly generated BA-Graph.

    """
    tarray = np.linspace(0,tf,Nt+1)
    # BA network with n nodes and m edges added when ever a new node is introduced
    G = nx.barabasi_albert_graph(n, m, seed)

    # the dictionary of node attributes, a list containing the S, I, V
    stat_dict = {k:[0, 0, 1] for k in list(G.nodes())}
    nx.set_node_attributes(G, stat_dict, 'status')
    # add degree to the network as well
    nx.set_node_attributes(G, dict(G.degree()), 'degree')

    # randomly choosing initially-infected node for fig3 and fig4
    x = np.random.choice(100, 1)[0]
    G.nodes[x]['status'] = [0.05, 0.05, 0.1]

    # configurarte plot parameters
    pltparams = {'legend.fontsize': 'xx-small',
         'axes.labelsize': 'x-small',
         'axes.titlesize':'x-small',
         'xtick.labelsize':'xx-small',
         'ytick.labelsize':'xx-small'}
    plt.rcParams.update(pltparams)

    """ We first look at the difference of mean and variance between linear diffusion
    and simplified infection model for varying conversion rate between infected cells
    and vulnerable cells."""
    plt.rcParams["figure.figsize"] = [20,10]
    plt.figure()
    for eval in range(4):
        for nvar in range(3):
            pos = 4*nvar + eval + 1
            var = ["S", "I", "V"][nvar]
            plot_theta(G,x,nvar,var,n,tarray,pos)
        if eval%2==0:
            plt.xlabel('time step/ Linear diffusion with x0={0} with degree {1}'.format(x, G.nodes[x]['degree']))
        else:
            plt.xlabel('time step/ simplfied infection model with x0={0} with degree {1}'.format(x, G.nodes[x]['degree']))
    plt.tight_layout()
    plt.subplots_adjust(top=0.92)
    plt.suptitle(r"Change of $\theta_0$ over BA Network ({0},{1}); Yidan Xu; modelN2, plot_theta".format(n,m))
    plt.savefig("fig0.png")
    plt.show()
    plt.close()


    """ Then, we investigate for the change of tau and a fixed theta0=80 as the
    rate of change from vulnerable to infected cells, comparing linear diffusion model
    and simplified infection model."""
    plt.rcParams["figure.figsize"] = [20,10]
    plt.figure()
    for eval in range(4):
        for nvar in range(3):
            pos = 4*nvar + eval + 1
            var = ["S", "I", "V"][nvar]
            plot_tau(G,x,nvar,var,n,tarray,pos)
        if eval%2==0:
            plt.xlabel('time step/ Linear diffusion with x0={0} with degree {1}'.format(x, G.nodes[x]['degree']))
        else:
            plt.xlabel('time step/ simplfied infection model with x0={0} with degree {1}'.format(x, G.nodes[x]['degree']))
    plt.tight_layout()
    plt.subplots_adjust(top=0.92)
    plt.suptitle(r"Change of Diffusivity $\tau$ over BA Network ({0},{1}); Yidan Xu; modelN2, plot_tau".format(n,m))
    plt.savefig("fig1.png")
    plt.show()
    plt.close()

    """ Consider the diffusion in the network as time progresses for the linear
    diffusion model and simplified infection model"""
    G.nodes[x]['status'] = [0.05, 0.05, 0.1]
    # for linear diffusion model
    eval = 1
    S = modelN2(G,eval,x,params,tf,Nt)[:, :n]

    plt.rcParams["figure.figsize"] = [20,10]
    plt.figure()
    for t in range(6):
        plot_t(S,n,t,eval)
        plt.ylabel("S in Linear Diffusion Model")

    eval = 2
    S = modelN2(G,eval,x,params,tf,Nt)[:, :n]
    for t in range(6):
        plot_t(S,n,t,eval)
        plt.ylabel("S in Simplified Infection Model")

    plt.tight_layout()
    plt.subplots_adjust(top=0.92)
    plt.suptitle("Network diffusion along time over BA Network ({0},{1}); Yidan Xu; modelN2, plot_t".format(n,m))
    plt.savefig("fig2.png")
    plt.show()
    plt.close()


if __name__=='__main__':
    # generate BA random graph
    n=100
    m=5
    seed=111
    G = nx.barabasi_albert_graph(n, m, seed)

    # the dictionary of node attributes, a list containing the S, I, V
    stat_dict = {k:[0, 0, 1] for k in list(G.nodes())}
    nx.set_node_attributes(G, stat_dict, 'status')
    # add degree to the network as well
    nx.set_node_attributes(G, dict(G.degree()), 'degree')

    # randomly choosing initially-infected node for fig3 and fig4
    x = np.random.choice(100, 1)[0]
    G.nodes[x]['status'] = [0.05, 0.05, 0.1]

    # run Question 1 with display of figure
    model1(G,x,params=(50,80,105,71,1,0),tf=6,Nt=400,display=True)
    # run Question 2 with display of figure
    modelN(G,x,params=(50,80,105,71,1,0.01),tf=6,Nt=400,display=True)

    # plot figures for Question 3
    diffusion(n=100,m=5,seed=111,params=(0, 80, 0, 0, 0, 0.01),tf=100,Nt=1000)
