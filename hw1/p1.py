"""M345SC Homework 1, part 1
Name: Yidan Xu
CID: 01205667
"""

def ksearch(S,k,f,x):
    """
    Search for frequently-occurring k-mers within a DNA sequence
    and find the number of point-x mutations for each frequently
    occurring k-mer.
    Input:
    S: A string consisting of A,C,G, and Ts
    k: The size of k-mer to search for (an integer)
    f: frequency parameter -- the search should identify k-mers which
    occur at least f times in S
    x: the location within each frequently-occurring k-mer where
    point-x mutations will differ.

    Output:
    L1: list containing the strings corresponding to the frequently-occurring k-mers
    L2: list containing the locations of the frequent k-mers
    L3: list containing the number of point-x mutations for each k-mer in L1.

    Discussion:
    We start by considering every length k sub-strings of DNA string S.
    Iterate through S and move to the right one element at a time.
    Store each k-mer as key with value the list of starting positions of the
    k-mer. Then, L1 can be constructed by iterate through all k-mers
    identified and hashed in the dictionary, and compare its occurrence
    frequencies (length of the list) with f.

    L2 and L3 are empty if L1 is empty. If it is not the case, L2 can
    be constructed accordingly by looking up the k-mers from L1 in hash
    table, and appending the lists of locations.

    As the point-x mutations only have three possible cases, we consider
    each of the possibility by specifying the xth position to be 'A',
    'C', 'G' and 'T'. Then, we find the corresponding lists of locations
    from the hash table if mutation exists; the occurance of x-point mutations
    is the sum of all occurrence frequencies of possible mutations (in hash table)
    minus the self-occurred frequency for k-mers in L1.


    Efficiency Analysis:

      Worst Case Scenario: all k-mers are frequently occurring k-mer,
      i.e. length of L1 is N-k+1, where N is the length of sequence S
      and k is the length of k-mer.
    ============================================================================
      103-108 for i in range(len(S)-k+1): ------------------------------ (N-k+1)
                    kmer = S[i:(i+k)] ---------------------------------- k
                    update hash table ---------------------------------- 4
      ------------------------------------------------------------------ (N-k+1)*(k+4)

      111 find frequently occurring k-mers ----------------------------- (N-k+1)*4

      115 find locations of L1 k-mers (L2) ----------------------------- (N-k+1)*2

      119-125 for i in range(len(L1)): --------------------------------- (N-k+1)
                    lmer = L1[i] --------------------------------------- 1
                    list of all mutations ------------------------------ 4(k+2)
                    find and append x-point mutation frequencies ------- 5*4*4+3+1=84
      ------------------------------------------------------------------ (N-k+1)(4k+87)
    ============================================================================
      The total time complexity of the implementation is (with c constant, occured
      from unaccounted operations in the implementation outside of loops):
        (N-k+1)*(k+4) + (N-k+1)*4 + (N-k+1)*2 + (N-k+1)(4k+87) + c
      = -5k**2 + (5N-92)*k + 97(N+1) + c
      = (5k+97)N - 92k - 5k**2 + 97 + c
    ============================================================================

    From the above calculation, if we regard k as variable and N as constant (and c
    negligible), we see the complexity is a concave quadratic function, with the
    greatest amount of time spent when k = 0.5N - 9.2; and the largest time
    complexity is 1.25N**2 + 51N + 520.2, which has leading order complexity O(N**2).
    Now we consider when k << N, the time complexity is linear O(N), however the
    coefficient of N is large, which is 5k+97. On the other hand, when k is
    approximately N, the complexity is again linear O(N), which is 5N+97.
    By and large, the leading order complexity is linear O(N) if k is regarded as
    constant. However, the value of k is proven to be essential in estimating
    the running time of the implementation as well.

    One alternative implementation of L3 is by removing the x-point from each k-mers
    in the hash table, which are then hashed with values of occurrence frequencies.
    The xth element is also removed for k-mers in L1, which are used to extract
    frequencies from the aforementioned new hash table; L3 is obtained by
    further subtracting the self-occurred frequencies for each k-mer in L1.
    The time complexity of this implementation is (2k+13)(N-k+1).

    In spite of similar theoretical time complexity, the running time of this
    implementation is longer on average. This could be due to that, under average
    cases, the length of L1 is normally shorter than N-k+1. Thus we have better
    empirical performance from the first implementation.
    """
    # warning: x must smaller than k
    assert x<k, "error, x must be smaller than k"

    # initialising
    dict_mer = dict()

    # loop over the string S
    # hash all possible k-mers
    for i in range(len(S)-k+1):
        kmer = S[i:(i+k)]
        if kmer in dict_mer:
            dict_mer[kmer] += [i]
        else:
            dict_mer[kmer] = [i]

    # extract objects with frequency occurring greater than f in dict_mer
    L1 = [key for (key, val) in dict_mer.items() if len(val)>=f]

    # if no k-mer is found, return empty lists for L2 and L3
    if len(L1)>0:
      L2 = [dict_mer.get(key) for key in L1]

      # find the four possible x-mutation for k-mers in L1
      L3 = []
      for i in range(len(L1)):
          lmer = L1[i]
          xmut = [lmer[:x] + mut + lmer[x+1:] for mut in ['A', 'C', 'G', 'T']]
          # the sum for frequencies of all possible mutations in dict_mer
          L3.append( sum([len(dict_mer.get(mut))
                                              for mut in xmut if mut in dict_mer])
                                                                    - len(L2[i]))

    # return empty L2 and L3 if L1 empty
    else:
      L2 = []
      L3 = []

    return L1,L2,L3

def ksearch_time(nS, k, f, x):
    """
    Input:
    nS: The length of a string consisting of A,C,G, and Ts
    k: The size of k-mer to search for (an integer)
    f: frequency parameter -- the search should identify k-mers which occur at
    least f times in S
    x: the location within each frequently-occurring k-mer where point-x
    mutations will differ.

    Output: fig0.png, a plot for the running time of `ksearch` with increasing
    k from x+1 to the length of the sequence S;

    Discussion:
    By empirical experiments in `ksearch_time`, we see the efficiency indeed has a
    concave quadratic shape as discussed in `ksearch` above. The implementation
    runs faster for k small and k approximately N=nS.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import time

    Ltime = []
    # randomly generate S
    S = "".join(np.random.choice(["A", "C", "G", "T"], size = nS))

    for k in range(x+1, nS):

        t1 = time.time()
        ksearch(S,k,f,x)
        t2 = time.time()
        Ltime.append(t2-t1)

    plt.figure()
    plt.plot(np.arange(x+1, nS), Ltime)
    plt.title(
            "Empirical Running Time against Increasing k; Yidan Xu; ksearch_time")
    plt.savefig("fig0.png")
    plt.close()

if __name__=='__main__':
    # -------------------------------------------------------------------------
    # test case for the implementation ----------------------------------------
    # -------------------------------------------------------------------------
    S = 'CCCTATGTTTGTGTGCACGGACACGTGCTAAAGCCAAATATTTTGCTAGT'
    k = 3
    x = 2
    f = 2
    L1, L2, L3 = ksearch(S, k, f, x)

    # -------------------------------------------------------------------------
    # empirical experiment of running time with increasing k for fixed N=nS.---
    # -------------------------------------------------------------------------
    nS = 5000
    k = 3
    x = 2
    f = 2
    ksearch_time(nS, k, f, x)
