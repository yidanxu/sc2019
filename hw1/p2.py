"""M345SC Homework 1, part 2
Name: Yidan Xu
CID: 01205667
"""

def nsearch(L,P,target):
    """Input:
    L: list containing *N* sub-lists of length M. Each sub-list
    contains M numbers (floats or ints), and the first P elements
    of each sub-list can be assumed to have been sorted in
    ascending order (assume that P<M). L[i][:p] contains the sorted elements in
    the i+1th sub-list of L
    P: The first P elements in each sub-list of L are assumed
    to be sorted in ascending order
    target: the number to be searched for in L

    Output:
    Lout: A list consisting of Q 2-element sub-lists where Q is the number of
    times target occurs in L. Each sub-list should contain 1) the index of
    the sublist of L where target was found and 2) the index within the sublist
    where the target was found. So, Lout = [[0,5],[0,6],[1,3]] indicates
    that the target can be found at L[0][5],L[0][6],L[1][3]. If target
    is not found in L, simply return an empty list (as in the code below)

    Code Structure and Algorithm:
    We treat the first P sorted values separately from the rest unsorted. And the
    following is implemented to each sub-lists in L.

    First P elements:
        Check if the target is out of the range of the first P elements in the
        sub-list. If so, proceed to the next sub-list. If not, we search for
        the lower index bound of the target then search for the upper index
        bound. We achieve this by modifying the binary search algorithm.
        Update the iend = imid-1 when target is found at imid, which continues the
        search to the left. Continue until iend == istart condition is met.
        Similarly, we can find the lower index bound. Check that we indeed have found
        the target in the sub-list, then we append indexes between lower and upper
        bound to Lout.

    P+1 to M elements:
        Use linear search append each index of found target to Lout.

    Efficiency Analysis:

      We assume that N, P, M-P is large. Worst cases scenario: the first P numbers
      are all target values.
    ================================================================================
      92 for i in range(nL): ---------------------------------------------- N

      113-123 find lower bound of target ---------------------------------- log(P)
                number of operations -------------------------------------- 6
      --------------------------------------------------------------------- 6*log(P)

      130-140 find upper bound of target ---------------------------------- log(P)
                number of operations -------------------------------------- 6
      --------------------------------------------------------------------- 6*log(P)

      143-145 append locations ------------------------------------------ 4+2P

      151-153 linear search ----------------------------------------------- (M-P)
                check if is target and assign ----------------------------- 5
      --------------------------------------------------------------------- 5*(M-P)
    ================================================================================
      The total time complexity of the implementation is (with c constant, occured
      from unaccounted operations in the implementation outside of loops):
      N*(6*log(P) + 6*log(P) + 4 + 2P + 5*(M-P) + c)
    = N*(12log(P) - 3P + 5M + 4 + c)
    ================================================================================

    From the above calculation, we see that the asymptotic time complexity is
    O(N*(log(P)+(M-P))); the running time is linear in M when N and P are fixed;
    linear in N when P and M are fixed. In the case that M and N are fixed, the
    time complexity varies according to (12log(P) - 3P), which is maximised when
    P=4, very small comparing to the large P we are interested in. Moreover, the
    running time decreases after P=4. As P increases faster than log(P), (-P)
    would become dominant for large P, and we should expect the running time to
    decrease almost linearly.

    On the contrary, if we implement linear search (e.g. linear search with stopping
    criterion when the next value is no longer a target; linear search from the
    front and end searching for the range of target position.), the running time
    would not depend on the value of P very much, but rather the total length of
    the sub-list M. Through empirical experimentation, we find that for linear
    search approach, holding M and N constant, the running time is fluctuating
    around 0.1-0.15 as P increases. And in this case, the leading coefficient
    complexity is O(MN). It is evident that the binary search approach performs
    better for P large, and the leading coefficient complexity is less than O(MN).
    The empirical running time analysis is further supported by plots in `nsearch_time`.
    """
    Lout = []
    nL = len(L)
    for i in range(nL):
        # within 1 sublist
        # for i in range(P):
        subL = L[i]
        # binary search for first P elements
        # if the target is smaller than the smallest, pass
        if target<subL[0]:
            pass
        # if the target is bigger than the largest, pass
        elif target>subL[P-1]:
            pass
        else:
            """ modified binary search =================================
            """
            istart = 0
            iend = P-1
            # t_loc = -1000

            # search for the lower bound of target
            Lbound = -1

            while istart <= iend:
                imid = (iend + istart)//2

                if target == subL[imid]:
                    Lbound = imid
                    # keep going left
                    iend = imid - 1
                elif target < subL[imid]:
                    iend = imid - 1
                else:
                    istart = imid + 1

            # search for the upper bound of target
            Ubound = -1
            istart = Lbound
            iend = P-1

            while istart <= iend:
                imid = (iend + istart)//2

                if target == subL[imid]:
                    Ubound = imid
                    # keep going right
                    istart = imid + 1
                elif target < subL[imid]:
                    iend = imid - 1
                else:
                    istart = imid + 1

            # update list if target is found
            if Lbound != -1 and Ubound != -1:
                for j in range(Lbound, Ubound+1):
                    Lout.append([i, j])


        """ linear search =============================================
        """
        # check for target and assign
        for j in range(P, len(subL)):
            if subL[j] == target:
                Lout.append([i,j])

    return Lout


def nsearch_time(nN, nM, fN, fM, fP, n=1000):
    """Analyze the running time of nsearch.
    Add input/output as needed, add a call to this function below to generate
    the figures you are submitting with your codes.

    Input:
    n: largest random number drawn in simulating L in `nsearch`.
    nN/nM: largest N/M used to simulate L when held others fixed.
    fN/fM/fP: N/M/P value that is held fixed when investigating a single r.v.

    Output:
    Plot1: running time for increasing N and fixed M and P, against the plot
    of theoratical time complexity.
    Plot2: running time for increasing M (M>fP) and fixed N and P, against the
    plot of theoratical time complexity.
    Plot3: running time for increasing P (fM>P) and fixed N and M, against the plot
    of theoratical time complexity.

    Discussion:
    We explore the trend in empirical running time by varying the values N, M, P
    one at a time, with others fixed. This is done by simulation with randomly
    generated L (with appropriate M and P) and target value. The generated plots
    corroborates with our analysis under function `nsearch`.

    Notably, the fluctuations of running time in the plots is influenced by the
    actual execution of the code, which is due to the functionality of computer
    and CPU. By averaging through several loops we are able to obtain a smoother
    presentation of the time, which would be overly time-consuming in generating
    the plot. We instead focus on analysing the general trend of the actual running
    time, and compare this with theoratical time complexity.

    In Plot1 (fig1.png), for increasing N and fixed M and P, the running time
    increases linearly, which is coherent to the theoretical time complexity
    O(N*(log(P)+(M-P))) as contrasted in the two subplots. This is obvious to
    see, as we have looped over the N sub-lists in the `nsearch` implementation.

    In Plot2 (fig2.png), for increasing M (M>fP) and fixed N and P, the running
    time is again increasing in a linear fashion. The empirical running time
    resembles that of the theoretical line. In this case, the time complexity
    only depends on the linear term M, with N and P held constant.

    In Plot3 (fig3.png), notably, the empirical and theoretical running time
    decreases almost linearly (not linear when P is small). And for
    O(N*(log(P)+(M-P))), P (negative value) is the leading order when N and M
    held fixed. Thus, while N and M are held fixed, running time decreases as
    P increases.
    """
    import matplotlib.pyplot as plt
    import numpy as np
    import time
    import math

    # make sure nM > fP
    assert fM > fP, "error, length of sub-list must be greater than fP."
    assert nM > fP, "error, length of sub-list must be greater than fP."

    # store empirical running time for each random variable
    lis_empt = []
    """ Running time for increasing N and fixed M and P ================
    """
    lis_timeN = []
    for N in range(1, nN+1):
        L = []
        # randomly choose a target value
        target = np.random.randint(1, n)

        for i in range(N):
            L.append(list(np.sort(np.random.randint(1, n, fP))) +
                     list(np.random.randint(1, n, fM-fP)))

        t1 = time.time()
        nsearch(L, fP, target)
        t2 = time.time()

        lis_timeN.append(t2-t1)

    lis_empt.append(lis_timeN)

    """ Running time for increasing M and fixed N and P ================
    """
    lis_timeM = []

    for M in range(fP, nM+1):
        L = []
        # randomly choose a target value
        target = np.random.randint(1, n)

        for i in range(fN):
            L.append(list(np.sort(np.random.randint(1, n, fP))) +
                     list(np.random.randint(1, n, M-fP)))

        t1 = time.time()
        nsearch(L, fP, target)
        t2 = time.time()

        lis_timeM.append(t2-t1)

    lis_empt.append(lis_timeM)

    """ Running time for increasing P and fixed M and N ================
    """
    lis_timeP = []

    for P in range(1, fM):
        L = []
        # randomly choose a target value
        target = np.random.randint(1, n)

        for i in range(fN):
            L.append(list(np.sort(np.random.randint(1, n, P)))
                             + list(np.random.randint(1, n, fM-P)))

        t1 = time.time()
        nsearch(L,P,target)
        t2 = time.time()

        lis_timeP.append(t2-t1)

    lis_empt.append(lis_timeP)

    """ Plot of running time for increasing N/M/P ======================
    """
    lis_x = []
    lis_thet = []
    lis_rv = ["N", "M", "P"]

    # Increasing N against empirical and theoretical running time
    lis_N = np.arange(1, nN+1)

    lis_x.append(lis_N)
    lis_thet.append(np.array([N*(12*math.log2(fP) - 3*fP + 5*fM) for N in lis_N]))

    # Increasing M against empirical and theoretical running time
    lis_M = np.arange(fP, nM+1)

    lis_x.append(lis_M)
    lis_thet.append(np.array([fN*(12*math.log2(fP) - 3*fP + 5*M) for M in lis_M]))

    # Increasing P against empirical and theoretical running time
    lis_P = np.arange(1, fM)

    lis_x.append(lis_P)
    lis_thet.append(np.array([fN*(12*math.log2(P) - 3*P + 5*fM) for P in lis_P]))

    for i in range(3):
        plt.figure()
        f, axarr = plt.subplots(2, sharex=True)

        axarr[0].plot(lis_x[i], lis_empt[i])
        axarr[0].set_title(
                "Empirical Running Time against Increasing {}; Yidan Xu; nsearch_time".format(lis_rv[i]))
        axarr[1].plot(lis_x[i], lis_thet[i])
        axarr[1].set_title(
                "Theoretical Running Time against Increasing {}; Yidan Xu; nsearch_time".format(lis_rv[i]))
        axarr[1].set_xlabel("{}".format(lis_rv[i]))

        f.savefig("fig{}.png".format(i+1))
        plt.close()

if __name__ == '__main__':

    # modify configuration as needed
    nN = 1000
    nM = 1100
    fN = 500
    fM = 1000
    fP = 600

    nsearch_time(nN, nM, fN, fM, fP, n=1000)
