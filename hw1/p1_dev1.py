"""M345SC Homework 1, part 1
Name: Yidan Xu
CID: 01205667
"""

def ksearch(S,k,f,x):
    """
    Search for frequently-occurring k-mers within a DNA sequence
    and find the number of point-x mutations for each frequently
    occurring k-mer.
    Input:
    S: A string consisting of A,C,G, and Ts
    k: The size of k-mer to search for (an integer)
    f: frequency parameter -- the search should identify k-mers which
    occur at least f times in S
    x: the location within each frequently-occurring k-mer where
    point-x mutations will differ.

    Output:
    L1: list containing the strings corresponding to the frequently-occurring k-mers
    L2: list containing the locations of the frequent k-mers
    L3: list containing the number of point-x mutations for each k-mer in L1.

    Discussion:
    Code Structure:
        Iterate through all length k substrings and assign starting position
        to hash table dict_mer.
        We assign the frequently-occurring k-mer to L1, whose number of positions
        stored in hash-table is greater than f.
        If no frequently occuring k-mer is found, L2 and L3 are empty.
        Otherwise, assign corresponding locations of frequently-occuring k-mer
        to L2. For L3, we first remove the xth character of the string in L1 and
        all k-mers found from S, then assign each shortened k-mers form S to a
        hash table dict_px with value the occurance frequency of such string in S.
        We iterate through the list of reduced strings of L1 as keys to extract
        the values in the hash table dict_px, then subtract the self-occurring
        frequency of k-mers in L1. Finally, the frequencies are assigned to L3,
        which are the number of occurance of point-x mutations of L1 k-mers.

    Efficiency Analysis
    !note: O(1) complexity for maintanance of hash table (search/add/delete)
    - L1: 1 assignment, 2 operations constructing range of for loop
          one loop: N-k; 4 operations in each loop for hash table maintenance
          one loop: N-k;
                        4 operations in each loop
        - worst case: 1+2+4(N-k)+4(N-k) = 3+8N-8k;
        O(N-k) complexity for N the length of S

    - 1 check for whether any frequently-occuring k-mer is found;
                        2 assignment if no such is found.

        Compute L2 and L3 if L1 not empty
        - L2: one loop: N-k;
                        2 operations in each loop
            - worst case: 2(N-k); O(N) complexity for N the length of S

        - L3: one loop: N-k;
                        k+1 operations in each loop

              2 operations constructing range of for loop
              one loop: N-k;
                        3 operations in each loop
              one loop: N-k;
                        3 operations in each loop

              3 operations constructing S_mer; 1 assignment of dict_px

              1 operation constructing range of for loop
              one loop: N-k;
                        6 operations in each loop for hash table maintenance
              1 operation constructing range of for loop
              one loop: N-k;
                        3+k operations in each loop
            - worst case: (k+1)(N-k)+2+3(N-k)+3(N-k)+3+1+1+6(N-k)+1+(3+k)(N-k) = 22N+8;
            O(N) complexity for N the length of S; quadratic in k (concave)

    For the function `ksearch`, worst case scenario we have total running time
    linear polynomial 3+8N+1+2N+22N+8=32N+12, which is O(N) asymptotically, for N
    large.
    """
    # warning: x must smaller than k
    assert x<k, "error, x must be smaller than k"

    # initialising
    dict_mer = dict()

    # loop over the string S
    # hash all possible k-mers
    for i in range(len(S)-k+1):
        kmer = S[i:(i+k)]
        if kmer in dict_mer:
            dict_mer[kmer] += [i]
        else:
            dict_mer[kmer] = [i]

    # extract objects with frequency occurring greater than f in dict_mer
    L1 = [key for (key, val) in dict_mer.items() if len(val)>=f]

    # if no k-mer is found, return empty lists for L2 and L3
    if len(L1)>0:
        L2 = [dict_mer.get(key) for key in L1]

        # to find the number of point-x mutation for each k-mer
        # remove the xth element from each k-mer in S and L1
        # S_del = [kmer[:x]+kmer[(x+1):] for kmer in list(dict_mer.keys())]
        # k-1 operation in slicing a k-1 string
        L1_del = [kmer[:x]+kmer[(x+1):] for kmer in L1]

        # all possible k-mer
        S_mer = list(dict_mer.keys())
        # locations for all k-mer identified
        S_mer_loc = list(dict_mer.values())

        # hash S_del with value the occurance frequency of original k-mer
        dict_px = dict()
        for i in range(len(S_del)):
            S_del_elt = S_mer[i][:x] + S_mer[i][(x+1):]
            if S_del_elt in dict_px:
                dict_px[S_del_elt] += len(S_mer_loc[i])
            else:
                dict_px[S_del_elt] = len(S_mer_loc[i])


        # accumulated frequency
        # disregarding self-occured frequency of k-mers in L1
        L3 = [dict_px.get(L1_del[i]) - len(L2[i]) for i in range(len(L1_del))]

    # return empty L2 and L3 if L1 empty
    else:
        L2 = []
        L3 = []

    return L1,L2,L3


if __name__=='__main__':

    S='CCCTATGTTTGTGTGCACGGACACGTGCTAAAGCCAAATATTTTGCTAGT'
    k=3
    x=2
    f=2
    L1,L2,L3=ksearch(S,k,f,x)
