def nsearch(L,P,target):
    Lout = []
    nL = len(L)
    for i in range(nL):
        # within 1 sublist
        # for i in range(P):
        subL = L[i]
        # binary search for first P elements
        # if the target is smaller than the smallest, pass
        if target<subL[0]:
            pass
        # if the target is bigger than the largest, pass
        elif target>subL[P-1]:
            pass
        else:
            """ Linear search, break till end of target sequence ==========
            """
            # Lbound = -1
            # Ubound = -1
            #
            # for j in range(P):
            #     if subL[j] == target:
            #         # update lower bound of target position
            #         Lbound = j
            #         break
            # for j in range(P):
            #     if subL[P-j-1] == target:
            #         # update upper bound of target position
            #         Ubound = j
            #         break
            # if Lbound != -1 and Ubound != -1:
            #     for j in range(Lbound, Ubound+1):
            #         Lout.append([i, j])

            # to indicate if we have reached
            flag = True
            for j in range(P):
                if flag:
                    if subL[j] == target:
                        Lout.append([i, j])
                        flag = subL[j+1] == target
                else:
                    break


            """ linear search =============================================
            """
            # check for target and assign
            for j in range(P, len(subL)):
                if subL[j] == target:
                    Lout.append([i, j])

    return Lout

def nsearch_time(nN, nM, fN, fM, fP, n=1000):
    """Analyze the running time of nsearch.
    Add input/output as needed, add a call to this function below to generate
    the figures you are submitting with your codes.

    Input:
    n: largest random number drawn in simulating L in `nsearch`.
    nN/nM: largest N/M used to simulate L when held others fixed.
    fN/fM/fP: N/M/P value that is held fixed when investigating a single r.v.

    Output:
    Plot1: running time for increasing N and fixed M and P, against the plot
    of theoratical asymptotic time complexity.
    Plot2: running time for increasing M (M>fP) and fixed N and P, against the
    plot of theoratical asymptotic time complexity.
    Plot3: running time for increasing P (fM>P) and fixed N and M, against the plot
    of theoratical asymptotic time complexity.

    Discussion:
    We explore the trend in empirical running time by varying the values N, M, P
    one at a time, with others fixed. This is done by simulation with randomly
    generated L (with appropriate M and P) and target value.

    In Plot1, for increasing N and fixed M and P, the running time increases
    linearly, which corroborates the theoretical time complexity O(N*(log(P)+(M-P)))
    as contrasted in the two subplots. This is obvious to see, as we have looped
    over the N sub-lists in the `nsearch` implementation.

    In Plot2, for increasing M (M>fP) and fixed N and P, the running time is
    again increasing in a linear fashion. The empirical running time resembles
    that of the theoretical line. In this case, the time complexity only depends
    on the linear term M, with N and P held constant.

    In Plot3, notably, the empirical and theoretical running time decreases linearly,
    as O(N*(log(P)+(M-P))) has leading order P, which is negative. Thus, while
    N and M are held fixed, running time decreases as P increases.
    """
    import matplotlib.pyplot as plt
    import numpy as np
    import time
    import math

    # make sure nM > fP
    assert fM > fP, "error, length of sub-list must be greater than fP."
    assert nM > fP, "error, length of sub-list must be greater than fP."

    # store empirical running time for each random variable
    lis_empt = []
    """ Running time for increasing N and fixed M and P ================
    """
    lis_timeN = []
    for N in range(1, nN+1):
        L = []
        # randomly choose a target value
        target = np.random.randint(1, n)

        for i in range(N):
            L.append(list(np.sort(np.random.randint(1, n, fP))) +
                     list(np.random.randint(1, n, fM-fP)))

        t1 = time.time()
        nsearch(L, fP, target)
        t2 = time.time()

        lis_timeN.append(t2-t1)

    lis_empt.append(lis_timeN)

    """ Running time for increasing M and fixed N and P ================
    """
    lis_timeM = []

    for M in range(fP, nM+1):
        L = []
        # randomly choose a target value
        target = np.random.randint(1, n)

        for i in range(fN):
            L.append(list(np.sort(np.random.randint(1, n, fP))) +
                     list(np.random.randint(1, n, M-fP)))

        t1 = time.time()
        nsearch(L, fP, target)
        t2 = time.time()

        lis_timeM.append(t2-t1)

    lis_empt.append(lis_timeM)

    """ Running time for increasing P and fixed M and N ================
    """
    lis_timeP = []

    for P in range(1, fM):
        L = []
        # randomly choose a target value
        target = np.random.randint(1, n)

        for i in range(fN):
            L.append(list(np.sort(np.random.randint(1, n, P)))
                             + list(np.random.randint(1, n, fM-P)))

        t1 = time.time()
        nsearch(L,P,target)
        t2 = time.time()

        lis_timeP.append(t2-t1)

    lis_empt.append(lis_timeP)

    """ Plot of running time for increasing N/M/P ======================
    """
    lis_x = []
    lis_thet = []
    lis_rv = ["N", "M", "P"]

    # Increasing N against empirical and theoretical running time
    lis_N = np.arange(1, nN+1)

    lis_x.append(lis_N)
    lis_thet.append(np.array([N*(math.log2(fP) + fM-fP) for N in lis_N]))

    # Increasing M against empirical and theoretical running time
    lis_M = np.arange(fP, nM+1)

    lis_x.append(lis_M)
    lis_thet.append(np.array([fN*(math.log2(fP) + M-fP) for M in lis_M]))

    # Increasing P against empirical and theoretical running time
    lis_P = np.arange(1, fM)

    lis_x.append(lis_P)
    lis_thet.append(np.array([fN*(math.log2(P) + fM-P) for P in lis_P]))

    for i in range(3):
        f, axarr = plt.subplots(2, sharex=True)

        axarr[0].plot(lis_x[i], lis_empt[i])
        axarr[0].set_title(
                "Empirical Running Time against Increasing {}; Yidan Xu; nsearch_time".format(lis_rv[i]))
        axarr[1].plot(lis_x[i], lis_thet[i])
        axarr[1].set_title(
                "Theoretical Running Time against Increasing {}; Yidan Xu; nsearch_time".format(lis_rv[i]))
        axarr[1].set_xlabel("{}".format(lis_rv[i]))

        f.savefig("fig_dev{}.png".format(i+1))


if __name__ == '__main__':

    # modify configuration as needed
    nN = 1000
    nM = 1100
    fN = 500
    fM = 1000
    fP = 600

    nsearch_time(nN, nM, fN, fM, fP, n=1000)
