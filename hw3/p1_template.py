"""M345SC Homework 3, part 1
Your name and CID here
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.signal import hann


def nwave(alpha,beta,Nx=256,Nt=801,T=200,display=False):
    """
    Question 1.1
    Simulate nonlinear wave model

    Input:
    alpha, beta: complex model parameters
    Nx: Number of grid points in x, frequency
    Nt: Number of time steps
    T: Timespan for simulation is [0,T]
    Display: Function creates contour plot of |g| when true

    Output:
    g: Complex Nt x Nx array containing solution
    """

    #generate equally-spaced grid
    L = 100
    x = np.linspace(0,L,Nx+1) # delx = L/Nx
    x = x[:-1]

    n = np.arange(-Nx/2, Nx/2, 1)
    jgrid = np.arange(0, Nx, 1)

    def RHS(f,t,alpha,beta):
        """Computes dg/dt for model eqn.,
        f[:N] = Real(g), f[N:] = Imag(g)
        Called by odeint below
        Output: Nx array containing df=dg/dt at time t
        """
        g = f[:Nx] + 1j*f[Nx:]
        # notably, d2g is also periodic
        # expand g(x,t) in terms of xj = j*delx, t fixed
        # note that xj is complex
        # use DFT to compute cn, for n = 0,...,Nx-1
        cn = np.fft.fft(g)
        cn_shift = np.fft.fftshift(cn)/Nx

        d2g = [(sum(n**2*np.exp(1j*2*np.pi*n*jn/Nx)*cn_shift)
                                  * -4 * np.pi**2/Nx**2) for jn in jgrid]

        dgdt = alpha*np.array(d2g) + g - beta*g*g*g.conj()
        df = np.zeros(2*Nx)
        df[:Nx] = dgdt.real
        df[Nx:] = dgdt.imag
        return df

    #set initial condition
    g0 = np.random.rand(Nx)*0.1*hann(Nx)
    f0=np.zeros(2*Nx)
    f0[:Nx]=g0
    t = np.linspace(0,T,Nt)

    #compute solution
    f = odeint(RHS,f0,t,args=(alpha,beta))
    g = f[:,:Nx] + 1j*f[:,Nx:]

    # discard initial transient
    g = g[50:,]
    t = t[50:,]

    if display:
        plt.figure()
        plt.contour(x,t,g.real)
        plt.xlabel('x')
        plt.ylabel('t')
        plt.title('Contours of Real(g)')

    return g

def analyze():
    """
    Question 1.2
    Add input/output as needed

    most energetic wavenumbers and frequencies ???
        as in Nx??

    Discussion: Add discussion here
    """

    """ Case A
    alpha = 1−2i, beta = 1+2i
    """
    alpha = 1 - 1j*2
    beta = 1 + 1j*2

    gA = nwave(alpha,beta,Nx=256,Nt=801,T=200,display=True)

    """ Case B
    alpha = 1-i, beta = 1+2i
    """
    alpha = 1 - 1j
    beta = 1 + 1j*2

    gB = nwave(alpha,beta,Nx=256,Nt=801,T=200,display=True)

    return None


def wavediff(Nx, w):
    """
    Question 1.3
    Nx: Number of grid points in x, frequency
    w: test wave at a particular time point

    Discussion: Add discussion here
    """
    L = 100
    x = np.linspace(0,L,Nx+1)
    x = x[:-1] # range is 0 to L-h
    h = L/Nx # the step

    # best 4th order
    ag = 0.5771439
    bg = 0.0896406
    a0 = 1.3025166
    b0 = 0.99335500
    c0 = 0.03750245

    # tridiagonal configuration (8th order)
    alpha = 3/8
    a1 = 25/16
    b1 = 1/5
    c1 = -1/80

    # one sided scheme (4th order)
    alpha2 = 3
    a2 = -17/6
    b2 = 3/2
    c2 = 3/2
    d2 = -1/6

    # construct A
    zv = np.ones(Nx)
    Ab = np.zeros([5,Nx])
    # not consider the first two rows' beta values
    Ab[0, 4:] = bg*zv[4:]
    # not consider the first two rows' alpha value
    Ab[1, 3:] = ag*zv[3:]
    Ab[2, :] = zv
    # not consider the last two rows' alpha values
    Ab[3, :-3] = ag*zv[3:]
    # not consider the last two rows' beta values
    Ab[4, :-4] = bg*zv[4:]

    # modify Ab, alpha terms for the 2nd and last 2nd row
    Ab[1, np.array([2, -1])] = alpha
    Ab[3, np.array([1, -3])] = alpha

    # modify Ab, alpha terms for the first and last row
    Ab[1, 1] = alpha2
    Ab[3, -2] = alpha2

    # construct LHS vector
    # intialise
    b = np.zeros(Nx)
    b[1:-1] = (w[2:] - w[:-2])*a0/(2*h) +
              (w[3:] - w[:-3])*b0/(4*h) +
              (w[4:] - w[:-4])*c0/(6*h)

    # modify for the 8th and one-side scheme
    b[0] = (a2*w[0] + b2*w[1] + c2*w[2] + d2*w[3])
    b[-1] = (a2*w[-1] + b2*w[-2] + c2*w[-3] + d2*w[-4])

    for j in [1, -2]:
        b[j] = (w[i+1] - w[i-1])*a0/(2*h) +
               (w[i+2] - w[i-2])*b0/(4*h) +
               (w[i+3] - w[i-3])*c0/(6*h)

    dgdx = scipy.linalg.solve_banded((2,2), Ab, b)

    return None

if __name__=='__main__':
    x=None
    #Add code here to call functions above and
    #generate figures you are submitting
